package com.example.firstapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    lateinit var rollButton: Button
    lateinit var resulImageView: ImageView
    lateinit var resulImageView2: ImageView
    var images= arrayOf( R.drawable.dice_1,R.drawable.dice_2,R.drawable.dice_3,R.drawable.dice_4,R.drawable.dice_5,R.drawable.dice_6)
    var diceNumber = 0
    var diceNumber2 = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        rollButton = findViewById(R.id.roll_button)
        resulImageView = findViewById(R.id.result_ImageView)
        resulImageView2 = findViewById(R.id.result_ImageView_2)

        rollButton.setOnClickListener{
            resulImageView.setImageResource(images[diceNumber])
            resulImageView2.setImageResource(images[diceNumber2])
            diceNumber= Random.nextInt(0,6)
            diceNumber2= Random.nextInt(0,6)
        }
    }
}